const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});
let menu = ['apel', 'jeruk', 'mangga']
console.log('Selamat Datang!\nMenu List');
for (let i = 0; i < menu.length; i++) {
    console.log(menu[i]);
}

function pilihMenu() {
    rl.question('pilih menu : ', (pilih) => {
        if (pilih == 'apel'|| pilih == 'jeruk' || pilih == 'mangga') {
            console.log(`anda memilih "${pilih}", selanjutnya pilih metode pembayaran : `)
            pilihMethod(pilih);
        } else {
            console.log('tidak ada di menu');        
        }
        return pilihMenu();
    })
}
function pilihMethod(pilih) {
    rl.question('pilih metode pembayaran : ', (bayar) => {
        if (bayar == 'debit' || bayar == 'cash') {
            console.log(`anda memilih "${bayar}", apakah pesanan ingin :`);
            makan(bayar);                
        } else {
            console.log('metode pembayaran ditolak!!!');
        }
        return pilihMethod(pilih);
    })
}
function makan(bayar) {
    rl.question('Take away or Dine in? ', (mam) => {
        if (mam == 'take away' || mam == 'dine in') {
            console.log(`anda memilih "${mam}."`);
            console.log("===================================");
            console.log("Terima Kasih, Silahkan Datang Kembali!")           
        } else {
            console.log("you can't do that here :)");
            return makan(bayar);
        }
        rl.close();
    })
}
pilihMenu();
pilihMethod();
makan();