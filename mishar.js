const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});
// flow pemesanan makanan resto korea  

let menu = ['sate', 'burger', 'nasgor', 'Lalapan']; // menu

// Function list of menu
function menu1() {
    console.log(`       Menu\nberikut adalah menu kami`);
    for (let i = 0; i < menu.length; i++) {
        console.log(` ${menu[i]} `);
    }
};

// // Function choice menu
// function pilihMenu(){
//   rl.question(`\nSilahkan pilih menu: `, (pilih) => {
//     if (pilih == 'sate' || pilih == 'burger' || pilih == 'nasgor' || pilih == 'lalapan') {
//       console.log(`\nMenu yang anda pesan adalah : ${pilih}`);
//       pilihBayar();
//     }else{
//       console.log(`\nSilahkan pilih menu yang tersedia`);
//       menu1();
//       pilihMenu();
//   }
//   }
// }


// coba menu
function pilihMenu () {
  rl.question(`\nSilahkan pilih menu yang anda inginkan\n'1' sate, '2' burger, '3' nasgor, '4' lalapan: `, (pilih)  => {
    if (pilih == 1){
      console.log('\njadi menu yang anda pilih adalah sate');
      pilihBayar();
    }else if (pilih == 2){
      console.log('\njadi menu yang anda pilih adalah burger');
      pilihBayar();
    }else if (pilih == 3){
      console.log('\njadi menu yang anda pilih adalah nasgor');
      pilihBayar();
    }else if (pilih == 4){
      console.log('\njadi menu yang anda pilih adalah lalapan');
      pilihBayar();
    }else {
      console.log('\nSilahkan pilih menu yang tersedia');
      menu1 ();
      pilihMenu();
    }
  })
}

// Function choice payment method
function pilihBayar(){
  rl.question(`\nSilahkan pilih metode pembayaran\n'1' untuk Cash, '2' untuk debit: `, (bayar) => {
    if(bayar == 1){
      console.log('\nSetelah Menerima Struk, silahkan melakukan pembayaran ke kasir');
      pilihMakan();
    }else if(bayar == 2){
      console.log('\nSilahkan masukan kartu Debit anda ke dalam mesin ini.');
      pilihMakan();
    }else{
      console.log('\nSilahkan memilih opsi pembayaran yang tersedia!');
      pilihBayar();
    }
  })
}

// Function Dine in or Take away
function pilihMakan(){
    rl.question(`\nPress '1' for dine in, press '2' for take away: `, (makan) => {
      if(makan == 1){
        console.log(`\nSilahkan mencari tempat duduk.`);
        console.log(`Thank you for the order `);
        console.log(`\nEnjoy your meal ^^`);
        rl.close();
      }else if(makan == 2){
        console.log('\nSilahkan tunggu makanan anda selesai disiapkan.');
        console.log(` Thank you for the order `);
        console.log(`\nEnjoy your meal ^^`);
        rl.close();
      }else{
        console.log(`\nHarap masukan opsi pilih makan yang tersedia.`);
        pilihMakan();
      }
    })
  }
  
  // Flow of function
  menu1();
  pilihMenu();
  pilihBayar();
  pilihMakan();
  